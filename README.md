Electrical sensor modules for smartRUE datalogger.
A set of AC and DC, voltage and current , sensor components provisioned for 
interconnection with the smartRUE datalogger core module, designed to comply 
with IEC specifications.

In this repository all the design issues are addressed, and reusable  schematic
and pcb design components are developed. 